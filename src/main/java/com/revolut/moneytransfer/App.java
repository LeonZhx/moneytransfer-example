package com.revolut.moneytransfer;

import com.revolut.moneytransfer.service.AccountService;
import com.revolut.moneytransfer.service.ServiceExceptionMapper;
import com.revolut.moneytransfer.service.TransactionService;
import com.revolut.moneytransfer.service.UserService;
import com.revolut.moneytransfer.dao.DAOManager;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args ) throws Exception
    {
      //System.out.println( "Hello World!" );
      DAOManager daoFactory = new DAOManager();
      daoFactory.populateTestData();

      Server server = new Server(8088);
  		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
  		context.setContextPath("/");
  		server.setHandler(context);
  		ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");
  		servletHolder.setInitParameter("jersey.config.server.provider.classnames",
  				UserService.class.getCanonicalName() + "," + AccountService.class.getCanonicalName() + ","
  						+ ServiceExceptionMapper.class.getCanonicalName() + ","
  						+ TransactionService.class.getCanonicalName());
  		try {
  			server.start();
  			server.join();
  		} finally {
  			server.destroy();
  		}
    }
}
