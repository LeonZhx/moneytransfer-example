package com.revolut.moneytransfer.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User {

    @JsonIgnore
    private long id;


    @JsonProperty(required = true)
    private String name;


    @JsonProperty(required = true)
    private String emailAddress;


    public User() {}

    public User(String userName, String emailAddress) {
        this.name = userName;
        this.emailAddress = emailAddress;
    }

    public User(long userId, String userName, String emailAddress) {
        this.id = userId;
        this.name = userName;
        this.emailAddress = emailAddress;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (!name.equals(user.name)) return false;
        return emailAddress.equals(user.emailAddress);

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + name.hashCode();
        result = 31 * result + emailAddress.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + emailAddress + '\'' +
                '}';
    }
}
