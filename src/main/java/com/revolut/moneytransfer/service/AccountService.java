package com.revolut.moneytransfer.service;

import com.revolut.moneytransfer.dao.DAOManager;
import com.revolut.moneytransfer.model.Account;
import com.revolut.moneytransfer.model.MoneyUtil;

import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Account Service
 */
@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountService {

    private final DAOManager daoFactory = new DAOManager();

    private static Logger log = Logger.getLogger(AccountService.class);


    /**
     * Find all accounts
     * @return
     * @throws Exception
     */
    @GET
    @Path("/all")
    public List<Account> getAllAccounts() throws Exception {
        return daoFactory.getAllAccounts();
    }

    /**
     * Find by account id
     * @param accountId
     * @return
     * @throws Exception
     */
    @GET
    @Path("/{accountId}")
    public Account getAccount(@PathParam("accountId") long accountId) throws Exception {
        return daoFactory.getAccountById(accountId);
    }

    /**
     * Find balance by account Id
     * @param accountId
     * @return
     * @throws Exception
     */
    @GET
    @Path("/{accountId}/balance")
    public BigDecimal getBalance(@PathParam("accountId") long accountId) throws Exception {
        final Account account = daoFactory.getAccountById(accountId);

        if(account == null){
            throw new WebApplicationException(/*"Account not found",*/ Response.Status.NOT_FOUND);
        }
        return account.getBalance();
    }

    /**
     * Create Account
     * @param account
     * @return
     * @throws Exception
     */
    @PUT
    @Path("/create")
    public Account createAccount(Account account) throws Exception {
        final long accountId = daoFactory.createAccount(account);
        return daoFactory.getAccountById(accountId);
    }

    /**
     * Deposit amount by account Id
     * @param accountId
     * @param amount
     * @return
     * @throws Exception
     */
    @PUT
    @Path("/{accountId}/deposit/{amount}")
    public Account deposit(@PathParam("accountId") long accountId,@PathParam("amount") BigDecimal amount) throws Exception {

        if (amount.compareTo(MoneyUtil.zeroAmount) <=0){
            throw new WebApplicationException(/*"Invalid Deposit amount",*/ Response.Status.BAD_REQUEST);
        }

        daoFactory.updateAccountBalance(accountId,amount.setScale(4, RoundingMode.HALF_EVEN));
        return daoFactory.getAccountById(accountId);
    }

    /**
     * Withdraw amount by account Id
     * @param accountId
     * @param amount
     * @return
     * @throws Exception
     */
    @PUT
    @Path("/{accountId}/withdraw/{amount}")
    public Account withdraw(@PathParam("accountId") long accountId,@PathParam("amount") BigDecimal amount) throws Exception {

        if (amount.compareTo(MoneyUtil.zeroAmount) <=0){
            throw new WebApplicationException(/*"Invalid Deposit amount",*/ Response.Status.BAD_REQUEST);
        }
        BigDecimal delta = amount.negate();
        if (log.isDebugEnabled())
            log.debug("Withdraw service: delta change to account  " + delta + " Account ID = " +accountId);
        daoFactory.updateAccountBalance(accountId,delta.setScale(4, RoundingMode.HALF_EVEN));
        return daoFactory.getAccountById(accountId);
    }


    /**
     * Delete amount by account Id
     * @param accountId
     * @param amount
     * @return
     * @throws Exception
     */
    @DELETE
    @Path("/{accountId}")
    public Response deleteAccount(@PathParam("accountId") long accountId) throws Exception {
        int deleteCount = daoFactory.deleteAccountById(accountId);
        if (deleteCount == 1) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
