package com.revolut.moneytransfer.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.revolut.moneytransfer.dao.DAOManager;
import com.revolut.moneytransfer.model.MoneyUtil;
import com.revolut.moneytransfer.model.Transaction;

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionService {

	private final DAOManager daoFactory = new DAOManager();

	/**
	 * Transfer fund between two accounts.
	 * @param transaction
	 * @return
	 * @throws Exception
	 */
	@POST
	public Response transferFund(Transaction transaction) throws Exception {

		String currency = transaction.getCurrencyCode();
		if (MoneyUtil.INSTANCE.validateCcyCode(currency)) {
			int updateCount = daoFactory.transferAccountBalance(transaction);
			if (updateCount == 2) {
				return Response.status(Response.Status.OK).build();
			} else {
				// transaction failed
				throw new WebApplicationException(/*"Transaction failed",*/ Response.Status.BAD_REQUEST);
			}

		} else {
			throw new WebApplicationException(/*"Currency Code Invalid ",*/ Response.Status.BAD_REQUEST);
		}

	}

}
