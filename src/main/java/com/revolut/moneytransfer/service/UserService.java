package com.revolut.moneytransfer.service;

import com.revolut.moneytransfer.dao.DAOManager;
import com.revolut.moneytransfer.model.User;

import org.apache.log4j.Logger;

import java.lang.String;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserService {

	private final DAOManager daoFactory = new DAOManager();

	private static Logger log = Logger.getLogger(UserService.class);

	/**
	 * Find by userName
	 * @param userName
	 * @return
	 * @throws Exception
	 */
    @GET
    @Path("/{userName}")
    public User getUserByName(@PathParam("userName") String userName) throws Exception {
        if (log.isDebugEnabled())
            log.debug("Request Received for get User by Name " + userName);
        final User user = daoFactory.getUserByName(userName);
        if (user == null) {
            throw new WebApplicationException(/*"User Not Found",*/ Response.Status.NOT_FOUND);
        }
        return user;
    }

    /**
	 * Find by all
	 * @param userName
	 * @return
	 * @throws Exception
	 */
    @GET
    @Path("/all")
    public List<User> getAllUsers() throws Exception {
        return daoFactory.getAllUsers();
    }

    /**
     * Create User
     * @param user
     * @return
     * @throws Exception
     */
    @POST
    @Path("/create")
    public User createUser(User user) throws Exception {
        if (daoFactory.getUserByName(user.getName()) != null) {
            throw new WebApplicationException(/*"User name already exist",*/ Response.Status.BAD_REQUEST);
        }
        final long uId = daoFactory.insertUser(user);
        return daoFactory.getUserById(uId);
    }

    /**
     * Find by User Id
     * @param userId
     * @param user
     * @return
     * @throws Exception
     */
    @PUT
    @Path("/{userId}")
    public Response updateUser(@PathParam("userId") long userId, User user) throws Exception {
        final int updateCount = daoFactory.updateUser(userId, user);
        if (updateCount == 1) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    /**
     * Delete by User Id
     * @param userId
     * @return
     * @throws Exception
     */
    @DELETE
    @Path("/{userId}")
    public Response deleteUser(@PathParam("userId") long userId) throws Exception {
        int deleteCount = daoFactory.deleteUser(userId);
        if (deleteCount == 1) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


}
