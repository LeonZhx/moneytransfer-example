--This script is used for unit test cases

DROP TABLE IF EXISTS User;

CREATE TABLE User (id LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
 UserName VARCHAR(30) NOT NULL,
 EmailAddress VARCHAR(30) NOT NULL);

CREATE UNIQUE INDEX idx_ue on User(UserName,EmailAddress);

INSERT INTO User (UserName, EmailAddress) VALUES ('leon','leon@gmail.com');
INSERT INTO User (UserName, EmailAddress) VALUES ('ada','ada@gmail.com');
INSERT INTO User (UserName, EmailAddress) VALUES ('chris','chris@gmail.com');

DROP TABLE IF EXISTS Account;

CREATE TABLE Account (id LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
UserName VARCHAR(30),
Balance DECIMAL(19,4),
CurrencyCode VARCHAR(30)
);



INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('leon',100.0000,'USD');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('ada',230.0000,'USD');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('leon',500.0000,'EUR');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('ada',500.0000,'EUR');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('leon',500.0000,'GBP');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('chris',500.0000,'GBP');
INSERT INTO Account (UserName,Balance,CurrencyCode) VALUES ('chris',300.0000,'AUD');
